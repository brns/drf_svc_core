# Django REST Service

Opinionated Setup for a Django REST Service. 

- Django 3.0
- Python 3.8



## Gettings started

For more infos please see the [Documentation](https://gonnado.gitlab.io/drf_svc_core/).


### Create local environment file

Create a `.env` file as a copy of `config/.env.template` in the root folder. 

``` shell
cp config/.env.template .env
```
This files is used by Django and Docker for configuration.


### Development database

#### Local postgres 

Assuming you have a local `postgres` up and running you can create a 
new development database using the following commands.  (Adjust names as needed.)

``` shell
sudo -u postgres psql -c "CREATE DATABASE drf_svc OWNER drf_svc ENCODING 'utf-8';"
sudo -u postgres psql -c "CREATE USER drf_svc SUPERUSER;"
```

And to delete them again

``` shell
DROP DATABASE drf_svc;
DROP USER drf_svc;
```

#### Docker 

You can also run postgres locally using Docker. 
The following starts a postgres container listening to port `localhost:5432`
with an initial database and user as defined in `.env`.

``` shell

sudo docker run -p 5432:5432 --env-file=.env --rm -d --name=postgres postgres

# Stop
sudo docker stop postgres

```


### Python Dependencies

The project uses `poetry` instead of `pip` to manage the dependencies. 
Usage is covered in the [Documentation](https://gonnado.gitlab.io/drf_svc_core/pages/template/development.html#dependencies).


### Init Django 

You will need a superuser and a OAuth Application (when connecting with a frontend client). 
 
``` shell
./manage.py createsuperuser 
./manage.py createapplication --client-id "AH43ZDIztHakpOB8A8mNnZ3j6o2qFyFXyCOImcBv" public password --skip-authorization
```
