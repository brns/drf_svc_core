import logging

from rest_framework import generics
from rest_framework import mixins
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.response import Response

from .models import User
from .serializers import CreateUserSerializer
from .serializers import UserSerializer

LOG = logging.getLogger(__name__)


class UserViewSet(
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):
    """Updates and retrieves user accounts"""

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAdminUser,)


class UserCreateViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """Creates user accounts"""

    queryset = User.objects.all()
    serializer_class = CreateUserSerializer
    permission_classes = (permissions.IsAdminUser,)


class AuthenticatedUserDetails(generics.RetrieveAPIView):
    """Provides access to the  details of the authenticated user."""

    permission_classes = [permissions.IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs) -> Response:
        """Return the user details of the current user.

        E.g. to display some user informations in a front-end client.
        :param request: Request # noqa: DAR101 *args **kwargs
        :returns: serialized user
        :rtype: Response
        """
        instance = request.user
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
