import datetime

from django.utils import timezone

import factory

from ..models import User


class ClientApplicationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "oauth2_provider.Application"
        django_get_or_create = ("name",)

    skip_authorization = True
    name = factory.Sequence(lambda n: f"Application {n}")
    client_secret = factory.Faker(
        "password",
        length=20,
        special_chars=True,
        digits=True,
        upper_case=True,
        lower_case=True,
    )
    client_id = factory.Faker(
        "password",
        length=20,
        special_chars=True,
        digits=True,
        upper_case=True,
        lower_case=True,
    )
    client_type = "public"
    authorization_grant_type = "password"
    redirect_uris = "http://127.0.0.1:8000"


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "core.User"
        django_get_or_create = ("username",)

    id = 1
    username = factory.Sequence(lambda n: f"testuser{n}")
    password = factory.Faker(
        "password",
        length=10,
        special_chars=True,
        digits=True,
        upper_case=True,
        lower_case=True,
    )
    email = factory.Faker("email")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    is_active = True
    is_staff = False

    @classmethod
    def _setup_next_sequence(cls):
        try:
            return User.objects.latest("id").id + 1
        except User.DoesNotExist:
            return 1


class TokenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "oauth2_provider.AccessToken"
        django_get_or_create = ("user", "application", "scope")

    expires = timezone.now() + datetime.timedelta(days=1)

    token = factory.Faker(
        "password",
        length=20,
        special_chars=True,
        digits=True,
        upper_case=True,
        lower_case=True,
    )
