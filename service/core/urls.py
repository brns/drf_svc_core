import logging

from django.conf import settings
from django.conf.urls import url
from django.urls import include
from django.urls import path
from django.urls import re_path
from django.urls import reverse_lazy
from django.views.generic.base import RedirectView
from rest_framework import routers

import oauth2_provider.views as oauth2_views

from .views import AuthenticatedUserDetails
from .views import UserCreateViewSet
from .views import UserViewSet

LOG = logging.getLogger(__name__)

# -----------------------------------------------------------------
# API
# -----------------------------------------------------------------

router = routers.DefaultRouter()
router.register(r"users", UserViewSet)
router.register(r"users", UserCreateViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("me/", AuthenticatedUserDetails.as_view(), name="me"),
    path(
        "api-auth/", include("rest_framework.urls", namespace="rest_framework")
    ),
    # the 'api-root' from django rest-frameworks default router
    # http://www.django-rest-framework.org/api-guide/routers/#defaultrouter
    # Redirect to gonnado ads
    re_path(
        r"^$",
        RedirectView.as_view(url=reverse_lazy("api-root"), permanent=False),
    ),
]

# OAuth2 provider endpoints
oauth2_endpoint_views = [
    url(
        r"^authorize/$",
        oauth2_views.AuthorizationView.as_view(),
        name="authorize",
    ),
    url(r"^token/$", oauth2_views.TokenView.as_view(), name="token"),
    url(
        r"^revoke-token/$",
        oauth2_views.RevokeTokenView.as_view(),
        name="revoke_token",
    ),
]

if settings.DEBUG:
    # OAuth2 Application Management endpoints
    oauth2_endpoint_views += [
        url(
            r"^applications/$",
            oauth2_views.ApplicationList.as_view(),
            name="list",
        ),
        url(
            r"^applications/register/$",
            oauth2_views.ApplicationRegistration.as_view(),
            name="register",
        ),
        url(
            r"^applications/(?P<pk>\d+)/$",
            oauth2_views.ApplicationDetail.as_view(),
            name="detail",
        ),
        url(
            r"^applications/(?P<pk>\d+)/delete/$",
            oauth2_views.ApplicationDelete.as_view(),
            name="delete",
        ),
        url(
            r"^applications/(?P<pk>\d+)/update/$",
            oauth2_views.ApplicationUpdate.as_view(),
            name="update",
        ),
    ]

    # OAuth2 Token Management endpoints
    oauth2_endpoint_views += [
        url(
            r"^authorized-tokens/$",
            oauth2_views.AuthorizedTokensListView.as_view(),
            name="authorized_token_list",
        ),
        url(
            r"^authorized-tokens/(?P<pk>\d+)/delete/$",
            oauth2_views.AuthorizedTokenDeleteView.as_view(),
            name="authorized_token_delete",
        ),
    ]
