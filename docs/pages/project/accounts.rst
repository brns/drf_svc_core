Resources and Accounts
=============================

Resource
------------

The resource abstraction uses `generic relations`_
to link a `UserProfile` with any matching class.

By now only the time of the last visit is tracked with it, but it could
be integrated with Djangos permission system for per-object permissions.

It uses a `UUID` as a primary key to allow referencing
copies of the resource in another service.

.. _`generic relations`: https://docs.djangoproject.com/en/3.0/ref/contrib/contenttypes/#generic-relations

.. autoclass:: service.accounts.models.AbstractResource
   :members: id, name, access
   :undoc-members: access


.. autoclass:: service.accounts.models.ResourceManager
   :members:

UserProfile
--------------------------
.. autoclass:: service.accounts.models.UserProfile
   :members: user, dark_theme, access


ResourcePermission
--------------------------
.. autoclass:: service.accounts.models.ResourceAccess
   :members: user_profile, resource

.. autoclass:: service.accounts.models.ResourceAccessManager
   :members: access
