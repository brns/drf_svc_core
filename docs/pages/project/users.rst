Users and Authentication
=============================

Views
-----------

.. automodule:: service.core.views
   :members:
   :undoc-members:

Utilities
--------------

.. autoclass:: service.core.management.commands.create_trusted_client.Command
   :members:

Permissions
--------------

.. automodule:: service.core.permissions
   :members:
   :undoc-members:


User Model
------------

.. automodule:: service.core.models
   :members:
   :undoc-members:
