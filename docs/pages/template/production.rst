Production
==========

The current environment is probably not the best suite for
big and high performance applications.
But should do the trick for small services.

In particular there is not yet a dedicated backend
for caching and as message brocker. For both the Django ORM is used.

The pipeline deploys to a pre-configured Kubernetes namespaces.
For the deployment the well documented `Component Chart`_ from DevSpace
is used.

.. _`Component Chart`: https://devspace.sh/component-chart/docs/

Another approach using ``docker-compose`` and ``caddy`` is given in
the `wemake-django-template`_ documentation.

.. _`wemake-django-template`: https://wemake-django-template.readthedocs.io/en/latest/pages/template/production.html#

Both approaches rely only on environmental variables for configuration
that are passed to the Gitlab pipeline.

Production configuration
------------------------

You will need to specify extra configuration
to run ``docker-compose`` in production.
Since production build also uses ``caddy``,
which is not required into the development build.

.. code:: bash

  docker-compose -f docker-compose.yml -f docker/docker-compose.prod.yml config > docker-compose.deploy.yml


Pulling pre-built images
------------------------

You will need to pull pre-built images from ``Gitlab`` to run them.
How to do that?

The first step is to create a personal access token for this service.
Then, login into your registry with:

.. code:: bash

   docker login registry.gitlab.your.domain

And now you are ready to pull your images:

.. code:: bash

   docker pull your-image:latest

See `official Gitlab docs <https://docs.gitlab.com/ee/user/project/container_registry.html>`_.


Updating already running service
--------------------------------

If you need to update an already running service,
them you will have to use ``docker service update``
or ``docker stack deploy``.

Updating existing `service <https://docs.docker.com/engine/reference/commandline/service_update/>`_.
Updating existing `stack <https://docs.docker.com/engine/reference/commandline/stack_deploy/>`_.

Zero-Time Updates
~~~~~~~~~~~~~~~~~

Zero-Time Updates can be tricky.
You need to create containers with the new code, update existing services,
wait for the working sessions to be completed, and to shut down old
containers.


Further reading
---------------

- Production with `docker-compose <https://docs.docker.com/compose/production>`_
- `Full tutorial <https://docs.docker.com/get-started>`_
