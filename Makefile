

# Export the .env file variables to use in scripts
env:
	export $(cat .env | xargs)

deploy:
	kubectl apply -f kubernetes/deployment.yaml
	kubectl apply -f kubernetes/job.yaml
	kubectl apply -f kubernetes/service.yaml

deploy_gitlab_review:
	kubectl create namespace ${RELEASE_TAG}
	kubectl config set-context --current --namespace=${RELEASE_TAG}
	helm upgrade ${RELEASE_TAG}-api ./chart  --set=image.tag=${RELEASE_TAG}


destroy_gitlab_review:
	kubectl config set-context --current --namespace=${RELEASE_TAG}

	kubectl delete -f kubernetes/deployment.yaml
	kubectl delete -f kubernetes/job.yaml
	kubectl delete -f kubernetes/service.yaml
	kubectl delete configmap release-config
	kubectl delete namespace ${RELEASE_TAG}

django-secret:

	kubectl create secret generic django-secrets \
		--from-literal=POSTGRES_PASSWORD='${POSTGRES_PASSWORD}' \
		--from-literal=DJANGO_SECRET_KEY='${SECRET DJANGO KEY}'
	# kubectl get secrets -o json --namespace old | jq '.items[].metadata.namespace = "new"' | kubectl create -f  -

namespace:
	kubectl create namespace ${K8S_NAMESPACE}
	kubectl config set-context --current --namespace=${K8S_NAMESPACE}

	kubectl create secret generic django-secrets \
		--from-literal=postgresql-password='' \
		--from-literal=secret-key='SECRET DJANGO KEY'
	# kubectl get secrets -o json --namespace old | jq '.items[].metadata.namespace = "new"' | kubectl create -f  -

	kubectl get secret gitlab-secret -o json --namespace api-review | jq '.metadata.namespace = "gd-core-test"' | kubectl create -f  -

	kubectl get secret django-secrets -o json --namespace api-review | jq '.metadata.namespace = "review"' | kubectl create -f  -

	kubectl get secret cloudflare-api-token-secret -o json --namespace api-review | jq '.metadata.namespace = "cert-manager"' | kubectl create -n cert-manager -f  -

	helm upgrade --install --reset-values --set=image.tag=${RELEASE_TAG} \
		--set="ingress.host=${RELEASE_TAG}.api.gonnado.com" \
		"${RELEASE_TAG}" ./chart



postgres:
	docker run --name postgres -p 5433:5432 -e POSTGRES_PASSWORD=db_pass
		-e POSTGRES_USER=postgres -e POSTGRES_DB=business -d postgres


ready:
	make format
	make clean
	make safe

format:
	@echo "\n----------------------------"
	@echo Reformat the code
	@echo "----------------------------\n"
	isort -rc .
	black service


clean:
	@echo "\n----------------------------"
	@echo Run tests and checks
	@echo "----------------------------\n"
	pytest
	mypy service
	flake8 service

safe:
	@echo "\n----------------------------"
	@echo Run deployment checks
	@echo "----------------------------\n"
	python manage.py makemigrations --dry-run --check
	python manage.py lintmigrations --exclude-apps django_q oauth2_provider

	@echo "Run deployment checks"
	safety check --full-report 
	yamllint .gitlab-ci.yml

        DJANGO_ENV=production python manage.py check --deploy --fail-level WARNING


doc:
	doc8 -q docs


venv:
	pip install virtualenv
	virtualenv .venv
	echo "Execute to activate: source .venv/bin/activate"

